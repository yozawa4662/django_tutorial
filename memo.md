# django tutorialメモ

## コマンド一覧
* python manage.py check
  * プロジェクトの問題を確認(DBのmigrations行わない)

* python manage.py makemigrations
  * migrationsの作成

* python manage.py migrate
  * migrationを行う

* python manage.py createsuperuser

* python manage.py runserver
  * サーバ起動

* python manage.py shell
  * shellでデバッグする

* python manage.py test polls
  * polls/test.pyに記述されているテストを実行

## テスト
* テストしたいメソッド名は"test"から始まる必要がある

## 用語
* migration: DBのデータを保持したままスキーマなどを変更する

## メモ
* settingsのTIME_ZONEをAsia/Tokyo、USE_TZをFalseにしないとサーバが日本の時間にならない
