# Djangoチュートリアル
* https://www.djangoproject.com/start/

## 作るもの
* Part1: 投票できてその結果を見れるサイトを作る
* Part2: 自分が投票の追加・変更・削除を行える管理者サイトを作る。

## メモ
* python mysite/manage.py runserver *port* でdevelopment serverが動く
  * 基本的に再起動はいらないが、ファイル追加時には必要とのこと
